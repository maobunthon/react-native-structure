import React, { Component } from "react";
import { Text, View } from "react-native";

class HomeView extends Component {
  render() {
    return (
      <View>
        <Text> This is home view </Text>
      </View>
    );
  }
}

export { HomeView };
export default HomeView;
