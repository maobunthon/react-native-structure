import React, { Component } from "react";
import { Text, View } from "react-native";

import { HomeView } from "./HomeView";

class GraphedHome extends Component {
  render() {
    return <HomeView />;
  }
}

export { GraphedHome };
export default GraphedHome;
