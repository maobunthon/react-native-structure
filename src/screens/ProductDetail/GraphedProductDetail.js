import React, { Component } from "react";
import { Text, View } from "react-native";

import { ProductListView } from "./ProductListView";

//Example how to call Row from components
// Note: you dont need to call direct from Row. just components is enough
import { Row } from "../../components";

class GraphedProductDetail extends Component {
  render() {
    return (
      <Row>
        <ProductListView />
      </Row>
    );
  }
}

export { GraphedProductDetail };
export default GraphedProductDetail;
