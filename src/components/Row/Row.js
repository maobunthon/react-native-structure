import styled from "styled-components";

const Row = styled.View`
  flex-direction: row;
`;

export { Row };
export default Row;
